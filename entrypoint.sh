#!/bin/sh

RESOURCES="/home/work/resources"
REVEALJS="$RESOURCES/reveal.js"
HIGHLIGHTJS="$RESOURCES/styles"

/home/tool/asciidoctor-revealjs /home/work/slides.adoc

if [ ! -d "$RESOURCES" ]; then
    mkdir -p $RESOURCES
fi

if [ ! -d "$REVEALJS" ]; then
    cp -R /home/tool/reveal.js $REVEALJS
fi

if [ ! -d "$HIGHLIGHTJS" ]; then
    cp -R /home/tool/styles $HIGHLIGHTJS
fi
