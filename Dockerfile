# STEP 1: Building
FROM docker.io/library/alpine:3.17.2 AS builder
RUN apk add --no-cache git wget unzip
WORKDIR /home/build

# Reveal.js
RUN git clone -b 3.9.2 --depth 1 https://github.com/hakimel/reveal.js.git
RUN rm -rf reveal.js/.git

# Highlight.js styles
COPY highlight.zip .
RUN unzip highlight.zip

# Asciidoctor-Reveal.js
RUN wget --quiet https://github.com/asciidoctor/asciidoctor-reveal.js/releases/download/v4.1.0/asciidoctor-revealjs-linux
RUN mv asciidoctor-revealjs-linux asciidoctor-revealjs


# STEP 2: Deployment
FROM docker.io/library/alpine:3.17.2

# https://wiki.alpinelinux.org/wiki/Running_glibc_programs
RUN apk add --no-cache gcompat libstdc++

WORKDIR /home/tool
COPY --from=builder /home/build/reveal.js ./reveal.js
COPY --from=builder /home/build/styles ./styles
COPY --from=builder /home/build/asciidoctor-revealjs .
COPY entrypoint.sh ./entrypoint
RUN chmod +x asciidoctor-revealjs && chmod +x entrypoint
ENTRYPOINT ["./entrypoint"]
